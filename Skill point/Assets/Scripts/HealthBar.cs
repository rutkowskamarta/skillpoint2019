﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public float changeBarSpeed;
    public Image imgHealthMask;
    public int maxHealth = 10;

    private float currentHealth;
    private float currentHealthPercentage;
    private float currentchangeBarSpeed;

    private void Start()
    {
        currentHealth = maxHealth;
        currentchangeBarSpeed = changeBarSpeed;
        setCurrentHealthPercentage();
        setHealthBarValues();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            ChangeHealthBy(-10);
        }
    }

    public float getCurrentHealth()
    {
        return currentHealth;
    }

    public void ChangeHealthBy(float changeValue)
    {
        StartCoroutine(ChangeHealthByCoroutine(changeValue));
    }

    private IEnumerator ChangeHealthByCoroutine(float changeValue)
    {
        float desiredHealth = currentHealth + changeValue;
        if (desiredHealth < 0)
            desiredHealth = 0;
        if (currentHealth >= maxHealth)
            currentHealth = maxHealth;

        while (currentHealth > desiredHealth)
        {
            currentHealth -= changeBarSpeed;
            if (currentHealth < desiredHealth)
            {
                currentHealth = desiredHealth;
            }
            setCurrentHealthPercentage();
            setHealthBarValues();

            yield return new WaitForEndOfFrame();
        }

    }

    private void setCurrentHealthPercentage()
    {
        currentHealthPercentage = (float)currentHealth / (float)maxHealth;
    }

    private void setHealthBarValues()
    {
        imgHealthMask.fillAmount = currentHealthPercentage;
    }
}
