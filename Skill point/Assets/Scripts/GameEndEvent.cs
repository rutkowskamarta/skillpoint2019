﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class GameEndEvent : MonoBehaviour
{
    public static GameEndEvent Instance;
    [SerializeField] private TextMeshProUGUI endgamePanelText;
    [SerializeField] private KnightAttackerController player1;
    [SerializeField] private KnightAttackerController player2;
    [SerializeField] private Color player1Color;
    [SerializeField] private Color player2Color;

    private void Awake()
    {
        if (Instance == null || FindObjectOfType<GameEndEvent>() == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        player1.loseEvent.AddListener(OnGameEnded);
        player2.loseEvent.AddListener(OnGameEnded);
    }

    private void OnGameEnded(KnightAttackerController winner)
    {
        StartCoroutine(GameEnded(winner));
    }

    public IEnumerator GameEnded(KnightAttackerController winner)
    {
        ShowWinnersPanel(winner);
        ChangeLoserToParticles(winner);
        yield return new WaitForSeconds(3f);
        MoveVictorTowardsPrincess(winner);
    }

    private IEnumerator ShowWinnersPanel(KnightAttackerController winner)
    {
        string text = $"Player {(winner == player1 ? "1" : "2")} won!";
        endgamePanelText.color = winner == player1 ? new Color(player1Color.r, player1Color.g, player1Color.b, 0f) : new Color(player2Color.r, player2Color.g, player2Color.b, 0f);
        while (endgamePanelText.color.a < 1f)
        {
            endgamePanelText.color = new Color(endgamePanelText.color.r, endgamePanelText.color.g, endgamePanelText.color.b, endgamePanelText.color.a + 0.1f);
            yield return new WaitForSeconds(0.2f);
        }
    }

    private void ChangeLoserToParticles(KnightAttackerController winner)
    {
        if (winner == player1)
            player2.ChangeToParticles();
        else
            player1.ChangeToParticles();
    }

    private void MoveVictorTowardsPrincess(KnightAttackerController winner)
    {
        if (winner == player1)
            player1.MoveTowardsPrincess();
        else
            player2.MoveTowardsPrincess();
    }
}
