﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dueler1Move : MonoBehaviour
{
    public float speed = 2;
    private int direction;

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A))
            direction = -1;
        else if (Input.GetKey(KeyCode.D))
            direction = 1;
        else
            direction = 0;
        transform.position = new Vector3(transform.position.x + speed * direction * Time.deltaTime, 0, 0);
    }
}
