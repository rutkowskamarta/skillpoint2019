﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(KnightPlayerController))]
public class BetterJumpScript : MonoBehaviour
{
    [SerializeField] private float jumpMultiplier = 2f;
    [SerializeField] private float fallMultiplier = 3f;
    [SerializeField] private Animator animator;

    private Rigidbody playerRigidbody;
    private bool isFalling = false;
    private bool isJumping = false;
    private KnightPlayerController playerController;

    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        playerController = GetComponent<KnightPlayerController>();
    }

    void Update()
    {
        BetterJump();
    }

    private void BetterJump()
    {
        if (playerRigidbody.velocity.y > 0 && playerController.isInTheAir)
        {
            playerRigidbody.velocity += Vector3.up * Physics.gravity.y * jumpMultiplier * Time.deltaTime;
            animator.SetBool("Jump", true);
            animator.SetBool("Fall", false);
        }
        else if (playerRigidbody.velocity.y < 0 && animator.GetBool("Jump")==true)
        {
            playerRigidbody.velocity += Vector3.up * Physics.gravity.y * fallMultiplier * Time.deltaTime;
            animator.SetBool("Jump", false);
            animator.SetBool("Fall", true);
            playerController.FeetCollider.enabled = true;
        }
    }
}
