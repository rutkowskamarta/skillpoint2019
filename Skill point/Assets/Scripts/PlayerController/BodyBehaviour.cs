﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using static WeaponController;

public class BodyBehaviour : MonoBehaviour
{
    [SerializeField] private KnightAttackerController secondKnightAttackerController;

    private KnightAttackerController myAttackerController;

    private void Awake()
    {
        myAttackerController = GetComponentInParent<KnightAttackerController>();
        Assert.IsNotNull(myAttackerController);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.tag == "Weapon")
        {
            WeaponController w = other.gameObject.GetComponentInChildren<WeaponController>();
            if (w.attackType==AttackType.High || !myAttackerController.isBlocking)
            {
                Debug.Log("Get damage");
                int damage = w.damage;
                float stuntTime = w.stuntTime;
                secondKnightAttackerController.TakeDamage(damage);
                secondKnightAttackerController.playerController.Stunt(stuntTime);
                w.ResetWeaponPower();
            }
            else
            {
                Debug.Log("Parying"); 
            }

        }
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.tag == "Weapon")
        {
            WeaponController w = other.gameObject.GetComponentInChildren<WeaponController>();
            if (w.attackType == AttackType.High || !myAttackerController.isBlocking)
            {
                Debug.Log("Get damage");
                int damage = w.damage;
                float stuntTime = w.stuntTime;
                secondKnightAttackerController.TakeDamage(damage);
                secondKnightAttackerController.playerController.Stunt(stuntTime);
                w.ResetWeaponPower();
            }
            else
            {
                Debug.Log("Parying");
            }
        }
    }


}
