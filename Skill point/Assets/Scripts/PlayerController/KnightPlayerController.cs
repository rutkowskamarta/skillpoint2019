﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Rigidbody))]
public class KnightPlayerController : MonoBehaviour
{
    [SerializeField] private Transform secondPlayerTransform;
    [SerializeField] private float forwardMovementSpeed; //x
    [SerializeField] private float forwardBlockMovementSpeed; //x
    [SerializeField] private float backMovementSpeed; //x
    [SerializeField] private float backBlockMovementSpeed; //x
    [SerializeField] private float jumpMovementSpeed; //x
    [SerializeField] private float jumpSpeed; //y
    [SerializeField] private float jumpCooldown;
    [SerializeField] private string joystickID;
    [SerializeField] private bool looksLeft;
    public Collider FeetCollider;
    [SerializeField] private Animator animator;
    private Rigidbody playerRigidbody;
    public bool isInTheAir = false;

    private bool isStunt = false;
    private float stuntDuration;
    [HideInInspector] public bool IsChangingColor = false;

    public bool canJump = true;

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        Assert.IsNotNull(animator);
    }

    void FixedUpdate()
    {
        if (!isStunt && !IsChangingColor)
        {
            LookTowardsSecondPlayer();
            PlayerJump();
            PlayerMovement();
        }
    }


    private void PlayerMovement()
    {
        float xSpeed = Input.GetAxis(joystickID + "Horizontal" + "Analog");
        if (Mathf.Abs(xSpeed) == 1)
        {

            animator.SetBool("Idle", false);

            if (xSpeed == 1 && looksLeft || xSpeed == -1 && !looksLeft)
            {
                animator.SetBool("Run", false);
                animator.SetBool("WalkBack", true);
                if (!animator.GetBool("Jump") && !animator.GetBool("Fall"))
                {
                    if(!animator.GetBool("Block"))
                        playerRigidbody.velocity = new Vector3(xSpeed * backMovementSpeed, playerRigidbody.velocity.y, 0);
                    else
                        playerRigidbody.velocity = new Vector3(xSpeed * backBlockMovementSpeed, playerRigidbody.velocity.y, 0);

                }
                else
                {
                        playerRigidbody.velocity = new Vector3(xSpeed * jumpMovementSpeed, playerRigidbody.velocity.y, 0);
                }
            }
            else if (xSpeed == 1 && !looksLeft || xSpeed == -1 && looksLeft)
            {
                animator.SetBool("WalkBack", false);
                animator.SetBool("Run", true);
                playerRigidbody.velocity = new Vector3(xSpeed * forwardMovementSpeed, playerRigidbody.velocity.y, 0);
                if (!animator.GetBool("Jump") && !animator.GetBool("Fall"))
                {
                    if (!animator.GetBool("Block"))
                        playerRigidbody.velocity = new Vector3(xSpeed * forwardMovementSpeed, playerRigidbody.velocity.y, 0);
                    else
                        playerRigidbody.velocity = new Vector3(xSpeed * forwardBlockMovementSpeed, playerRigidbody.velocity.y, 0);
                }
                else
                {
                    playerRigidbody.velocity = new Vector3(xSpeed * jumpMovementSpeed, playerRigidbody.velocity.y, 0);
                }
            }
        }
        else
        {
            if (!isStunt)
                playerRigidbody.velocity = new Vector3(0, playerRigidbody.velocity.y, 0);
            animator.SetBool("Idle", true);
            animator.SetBool("WalkBack", false);
            animator.SetBool("Run", false);
        }
    }

    private void PlayerJump()
    {
        if (Input.GetButtonDown(joystickID + "A") && !isInTheAir && canJump)
        {
            playerRigidbody.velocity = new Vector3(playerRigidbody.velocity.x, jumpSpeed, 0);
            isInTheAir = true;
            canJump = false;
            FeetCollider.enabled = false;
            //TODO: Jumping, falling distinction and animation
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Ground"))
        {
            isInTheAir = false;
            animator.SetBool("Jump", false);
            animator.SetBool("Fall", false);
            ChangeJumpState();
        }
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.CompareTag("Ground"))
        {
            isInTheAir = false;
            animator.SetBool("Jump", false);
            animator.SetBool("Fall", false);
            ChangeJumpState();
        }
    }

    private bool isJumpCooldown;

    private void ChangeJumpState()
    {
        if (!isJumpCooldown && !canJump)
        {
            isJumpCooldown = true;
            Invoke("SetJumpStateTrue", jumpCooldown);
        }
    }

    private void SetJumpStateTrue()
    {
        canJump = true;
        isJumpCooldown = false;
    }
    private void OnGUI()
    {
        //GUI.Label(new Rect(10, 10, 200, 20), xSpeed.ToString());
    }

    private void LookTowardsSecondPlayer()
    {
        if (transform.position.x < secondPlayerTransform.position.x && looksLeft)
        {
            Rotate();
        }
        else if (transform.position.x >= secondPlayerTransform.position.x && !looksLeft)
        {
            Rotate();
        }
    }

    private void Rotate()
    {
        transform.eulerAngles = new Vector3(0, (transform.eulerAngles.y + 180) % 360, 0);
        looksLeft = transform.eulerAngles.y == 0;
    }

    public void Stunt(float stuntDuration)
    {
        this.stuntDuration = stuntDuration;
        StartCoroutine(StuntCoroutine());
    }

    private IEnumerator StuntCoroutine()
    {
        isStunt = true;
        yield return new WaitForSeconds(stuntDuration);
        isStunt = false;
    }

    public void BlockMovement()
    {
        IsChangingColor = true;
    }
}
