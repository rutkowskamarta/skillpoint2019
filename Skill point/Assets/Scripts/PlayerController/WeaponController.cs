﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public enum AttackType
    {
        High, Medium, Low
    }

    [HideInInspector] public int damage = 0;
    public float stuntTime = 0;

    public AttackType attackType;

    public void SetWeaponPower(int damage, float stuntTime, AttackType type)
    {
        this.attackType = type;
        this.damage = damage;
        this.stuntTime = stuntTime;
    }

    public void ResetWeaponPower()
    {
        damage = 0;
        stuntTime = 0;
    }

}
