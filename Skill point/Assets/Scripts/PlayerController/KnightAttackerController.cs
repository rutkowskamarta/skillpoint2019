﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using static WeaponController;

public class KnightAttackerController : MonoBehaviour
{
    [SerializeField] private KnightAttackerController secondPlayer;
    [SerializeField] private WeaponController weaponController;
    [SerializeField] private string joystickID;
    [SerializeField] public UnityEvent<KnightAttackerController> loseEvent;
    [SerializeField] private HealthBar healthBar;
    [SerializeField] private Animator animator;
    [SerializeField] private Transform princessTransform;

    [HideInInspector] public float healthPercantage = 100f;
    [HideInInspector] public KnightPlayerController playerController;

    private bool movesTowardsPrincess = false;

    public bool isBlocking;

    private void Awake()
    {
        playerController = GetComponent<KnightPlayerController>();
    }

    void Start()
    {
    }

    void Update()
    {
        if (!playerController.IsChangingColor)
        {
            LTDefense();
            XAttack();
            AAttack();
            YAttack();
        }
        if (movesTowardsPrincess)
        {
            Move();
        }
    }

    private void XAttack()
    {
        if (Input.GetButtonDown(joystickID + "X"))
        {
            animator.SetBool("AttackLow", true);
            animator.SetBool("AttackMedium", false);
            animator.SetBool("AttackHigh", false);
            animator.SetBool("Block", false);
            isBlocking = false;

            weaponController.SetWeaponPower(-10, 1, AttackType.Low);
        }
    }

    private void AAttack()
    {
        if (Input.GetButtonDown(joystickID + "B"))
        {
            animator.SetBool("AttackLow", false);
            animator.SetBool("AttackMedium", true);
            animator.SetBool("AttackHigh", false);
            animator.SetBool("Block", false);
            isBlocking = false;
            weaponController.SetWeaponPower(-15, 1, AttackType.Medium);
        }
    }

    private void LTDefense()
    {
        if (Mathf.Abs(Input.GetAxis(joystickID + "LT"))==1)
        {
            animator.SetBool("Block", true);
            //zmienna jakaś

        } else
        {
            animator.SetBool("Block", false);
            isBlocking = false;
        }
    }


    private void YAttack()
    {
        if (Input.GetButtonDown(joystickID + "Y"))
        {
            animator.SetBool("AttackLow", false);
            animator.SetBool("AttackMedium", false);
            animator.SetBool("AttackHigh", true);
            animator.SetBool("Block", false);
            isBlocking = false;
            weaponController.SetWeaponPower(-20, 1, AttackType.High);

        }
    }

    public void TakeDamage(int damage)
    {
        healthPercantage += damage;
        healthBar.ChangeHealthBy(damage);
        if (healthPercantage <= 0)
        {
            loseEvent.Invoke(this);
        }
    }

    public void ChangeToParticles()
    {
        //
    }

    public void MoveTowardsPrincess()
    {
        animator.SetBool("Walk", true);
        movesTowardsPrincess = true;
    }

    private void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, princessTransform.position, 0.1f);
    }
}
