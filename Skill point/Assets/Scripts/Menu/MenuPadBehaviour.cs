﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PAD_POSITION { LEFT, RIGHT, CENTER };

public class MenuPadBehaviour : MonoBehaviour
{
    public Image padImage;
    [HideInInspector] public PAD_POSITION playerPadPosition = PAD_POSITION.CENTER;
    [HideInInspector] public Vector3 centerPadPosition;
    [HideInInspector] public bool IsReady = false;
    

    void Start()
    {
        centerPadPosition = transform.position;
    }

  
}
