﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SideSelectionManager : MonoBehaviour
{
    [SerializeField] private MenuPadBehaviour player1Icon;
    [SerializeField] private MenuPadBehaviour player2Icon;
    
    [SerializeField] private GameObject leftHandle;
    [SerializeField] private GameObject rightHandle;

    [SerializeField] private Image leftImageButton;
    [SerializeField] private Image rightImageButton;

    [SerializeField] private Sprite bButtonSprite;
    [SerializeField] private Sprite aButtonSprite;

    [SerializeField] private Sprite bluePadSprite;
    [SerializeField] private Sprite redPadSprite;
    [SerializeField] private Sprite basicPadSprite;

    private int playerCount = 0;

    private Vector2 centerPadPosition2;
    private bool isLeftTaken = false;
    private bool isRightTaken = false;
    private float time1 = 0;
    private float time2 = 0;

    void Start()
    {
        UpdateAxisCount();
        leftImageButton.gameObject.SetActive(false);
        rightImageButton.gameObject.SetActive(false);
    }

    void Update()
    {
        UpdateAxisCount();
        SelectPlayer("Player1SideSelection", player1Icon, ref time1);
        SelectPlayer("Player2SideSelection", player2Icon, ref time2);
        AcceptChoice("Player1", player1Icon);
        AcceptChoice("Player2", player2Icon);
        DeclineChoice("Player1", player1Icon);
        DeclineChoice("Player2", player2Icon);
        HighLightProperImage();
        StartAGame();
    }

    private void UpdateAxisCount()
    {
        int newPlayerCount = CalculateNumberOnXboxControllers();

        if (newPlayerCount!=playerCount)
        {
            playerCount = newPlayerCount;
            OnJoystickCountChanged(playerCount);
        }
    }

    private int CalculateNumberOnXboxControllers()
    {
        int number = 0;
        foreach (var item in Input.GetJoystickNames())
        {
            if (item.Equals("Controller (Xbox 360 Wireless Receiver for Windows)"))
            {
                number++;
            }
        }
        return number;
        
    }

    public void OnJoystickCountChanged(int playerCount)
    {
        if (playerCount == 0)
        {
            player1Icon.gameObject.SetActive(false);
            player2Icon.gameObject.SetActive(false);
        }
        if(playerCount == 1)
        {
            player1Icon.gameObject.SetActive(true);
            player2Icon.gameObject.SetActive(false);
        }
        else if(playerCount == 2)
        {
            player1Icon.gameObject.SetActive(true);
            player2Icon.gameObject.SetActive(true);
        }
    }

    private void SelectPlayer(string axisName, MenuPadBehaviour pad, ref float time)
    {
        if (time < 0.5f)
            time += Time.deltaTime;
        else
        {
            if(!pad.IsReady)
                MakeMove(axisName, pad, ref time);
        }
       
    }

    private void MakeMove(string axisName, MenuPadBehaviour pad, ref float time)
    {
        float move = Input.GetAxis(axisName);
        if (move == -1)
        {
            if (pad.playerPadPosition == PAD_POSITION.CENTER && !isRightTaken)
            {
                pad.transform.position = rightHandle.transform.position;
                pad.playerPadPosition = PAD_POSITION.RIGHT;
                isRightTaken = true;
                time = 0;
                pad.padImage.sprite = redPadSprite;
            }
            else if (pad.playerPadPosition == PAD_POSITION.LEFT)
            {
                pad.transform.position = pad.centerPadPosition;
                pad.playerPadPosition = PAD_POSITION.CENTER;

                isLeftTaken = false;
                time = 0;
                pad.padImage.sprite = basicPadSprite;

            }
        }
        else if (move == 1)
        {
            if (pad.playerPadPosition == PAD_POSITION.CENTER && !isLeftTaken)
            {
                pad.transform.position = leftHandle.transform.position;
                pad.playerPadPosition = PAD_POSITION.LEFT;

                isLeftTaken = true;
                time = 0;
                pad.padImage.sprite = bluePadSprite;


            }
            else if (pad.playerPadPosition == PAD_POSITION.RIGHT)
            {
                pad.transform.position = pad.centerPadPosition;
                pad.playerPadPosition = PAD_POSITION.CENTER;

                isRightTaken = false;
                time = 0;
                pad.padImage.sprite = basicPadSprite;

            }
        }
    }

    private void HighLightProperImage()
    {
        leftImageButton.gameObject.SetActive(isLeftTaken);
        rightImageButton.gameObject.SetActive(isRightTaken);
    }

    private void AcceptChoice(string joystickID, MenuPadBehaviour pad)
    {
        if(Input.GetButtonDown(joystickID + "A") && !pad.IsReady)
        {
            if(pad.playerPadPosition == PAD_POSITION.LEFT)
            {
                pad.IsReady = true;
                leftImageButton.sprite = bButtonSprite;

            }
            else if(pad.playerPadPosition == PAD_POSITION.RIGHT)
            {
                pad.IsReady = true;
                rightImageButton.sprite = bButtonSprite;
            }
        }
    }

    private void DeclineChoice(string joystickID, MenuPadBehaviour pad)
    {
        if (Input.GetButtonDown(joystickID + "B") && pad.IsReady)
        {
            if (pad.playerPadPosition == PAD_POSITION.LEFT)
            {
                pad.IsReady = false;
                leftImageButton.sprite = aButtonSprite;

            }
            else if (pad.playerPadPosition == PAD_POSITION.RIGHT)
            {
                pad.IsReady = false;
                rightImageButton.sprite = aButtonSprite;
            }
        }
    }

    private void StartAGame()
    {
        if(player1Icon.IsReady && player2Icon.IsReady)
        {
            Initiate.Fade("SampleScene", Color.black, 2);
        }
    }
}
