﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuManager : MonoBehaviour
{
    public TextMeshProUGUI[] buttons;
    public Color selectedColor;
    public Color unselectedColor;
    public bool showAtStart;
    public float deadMovementTime = 0.1f;

    private int selectedButton = 0;
    private bool isShown;
    private bool isSelectionAxisInUse;
    private bool isEnterAxisInUse;
    private bool blockedAxis;

    void Start()
    {
        isShown = showAtStart;
        if (isShown)
            Show();
        else
            Hide();
                
        SelectButton(selectedButton);
    }

    public void SelectButton(int index)
    {
        selectedButton = index;
        for (int i=0; i<buttons.Length; i++)
        {
            buttons[i].color = i == selectedButton ? selectedColor : unselectedColor;
        }

    }



    public void Update()
    {
        if(isShown && !blockedAxis)
        {
            UpdateSelectionAxis();
            UpdateEnterAxis();
        }
    }


    private void UpdateSelectionAxis()
    {
        if (isSelectionAxisInUse && Mathf.Abs(Input.GetAxisRaw("MenuSelectionChange")) < 1)
        {
            blockedAxis = true;
            Invoke("DeadMovementTime", deadMovementTime);
        }
        else if (isSelectionAxisInUse == false)
        {
            isSelectionAxisInUse = true;
            UpdateSelectionChangeInput();
        }
    }

    private void UpdateEnterAxis()
    {
        if (isEnterAxisInUse && Mathf.Abs(Input.GetAxisRaw("MenuEnter")) == 0)
        {
            blockedAxis = true;
            Invoke("DeadMovementTime", deadMovementTime);
        }
        else if (isEnterAxisInUse == false)
        {
            isEnterAxisInUse = true;
            UpdateEnterInput();
        }
    }

    private void DeadMovementTime()
    {
        isSelectionAxisInUse = false;
        isEnterAxisInUse = false;
        blockedAxis = false;
    }

    private void UpdateSelectionChangeInput()
    {
        if (Input.GetAxis("MenuSelectionChange") < 0)
            SelectPreviousButton();
        else if (Input.GetAxis("MenuSelectionChange") > 0)
            SelectNextButton();
    }

    private void UpdateEnterInput()
    {
        if (Input.GetAxis("MenuEnter") != 0)
        {
            MenuButton currentButton = buttons[selectedButton].GetComponent<MenuButton>();
            if (currentButton != null)
                currentButton.OnButtonClicked.Invoke();
            else
                Debug.LogWarning("Button " + buttons[selectedButton].name + " doesn't have MenuButton component!");
        }
    }

    public void SelectPreviousButton()
    {
        int previousButtonIndex = Mathf.Clamp(selectedButton+1, 0, buttons.Length - 1);
        SelectButton(previousButtonIndex);
    }

    public void SelectNextButton()
    {
        int nextButtonIndex = Mathf.Clamp(selectedButton - 1, 0, buttons.Length-1);
        SelectButton(nextButtonIndex);
    }

    public void Show()
    {
        isShown = true;
        UpdateButtons();
    }

    public void Hide()
    {
        isShown = false;
        UpdateButtons();
    }

    private void UpdateButtons()
    {
        for (int i = 0; i < buttons.Length; i++)
            buttons[i].gameObject.SetActive(isShown);
    }
}
