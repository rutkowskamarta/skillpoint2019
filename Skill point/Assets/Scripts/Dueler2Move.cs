﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dueler2Move : MonoBehaviour
{
    public float speed = 2;
    private int direction;

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
            direction = -1;
        else if (Input.GetKey(KeyCode.RightArrow))
            direction = 1;
        else
            direction = 0;
        transform.position = new Vector3(transform.position.x + speed * direction * Time.deltaTime, 0, 0);
    }
}
