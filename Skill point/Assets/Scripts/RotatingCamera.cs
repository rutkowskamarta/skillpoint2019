﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingCamera : MonoBehaviour
/// RotatingCamera must be a child of rotationCenter
{
    public Transform dueler1, dueler2;
    public Transform rotationCenter;
    public bool rotateRotationCenter;

    void FixedUpdate()
    {
        float cameraX = (dueler1.position.x + dueler2.position.x) / 2;
        if (rotateRotationCenter)
        {
            Vector3 DuelCenterPoint = new Vector3(cameraX, 0, 0);
            rotationCenter.transform.LookAt(DuelCenterPoint);
            rotationCenter.eulerAngles = new Vector3(0, rotationCenter.eulerAngles.y, 0);
        } else
        {
            Vector3 newPosition = new Vector3(cameraX, transform.position.y, transform.position.z);
            transform.position = Vector3.Lerp(transform.position, newPosition, 0.5f);
        }
    }
}
