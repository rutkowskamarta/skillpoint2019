﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Animator))]
public class PlayerAnimationController : MonoBehaviour
{

    private Animator animator;
    public Collider swordCollider;
    private KnightAttackerController attackerController;

    void Start()
    {
        Assert.IsNotNull(swordCollider);
        animator = GetComponent<Animator>();
        attackerController = GetComponentInParent<KnightAttackerController>();
    }

    public void ResetHighAttack()
    {
        animator.SetBool("AttackHigh", false);
    }

    public void ResetMediumAttack()
    {
        animator.SetBool("AttackMedium", false);
    }

    public void ResetLowAttack()
    {
        animator.SetBool("AttackLow", false);
    }

    public void ActivateSwordCollider()
    {
        swordCollider.enabled = true;
    }

    public void DeactivateSwordCollider()
    {
        swordCollider.enabled = false;
    }

    public void StartBlocking()
    {
        attackerController.isBlocking = true;
    }

}
