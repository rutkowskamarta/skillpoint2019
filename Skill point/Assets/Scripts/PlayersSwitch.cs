﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayersSwitch : MonoBehaviour
{
    [SerializeField] private GameObject player1;
    [SerializeField] private GameObject player2;
    [SerializeField] private Transform particleSystem1;
    [SerializeField] private Transform particleSystem2;

    [SerializeField] private float yOffset;
    [SerializeField] private float initSmoothTime;
    [SerializeField] private float finalSmoothTime;
    [SerializeField] private float upDuration = 0.5f;

    [SerializeField] private UnityEvent resetTimerEvent;

    private KnightAttackerController knightAttackerController1;
    private KnightAttackerController knightAttackerController2;

    private KnightPlayerController knightPlayerController1;
    private KnightPlayerController knightPlayerController2;

    private Vector3 target1;
    private Vector3 target2;

    private bool shouldExecute = false;

    void Start()
    {
        knightAttackerController1 = player1.GetComponent<KnightAttackerController>();
        knightAttackerController2 = player2.GetComponent<KnightAttackerController>();

        knightPlayerController1 = player1.GetComponent<KnightPlayerController>();
        knightPlayerController2 = player2.GetComponent<KnightPlayerController>();

        target1 = player1.transform.position;
        target2 = player2.transform.position;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SwitchAppereance();
        }
    }

    public void SwitchAppereance()
    {
        shouldExecute = false;
        DisableMovement();
        StartCoroutine(MoveParticles(particleSystem1, target2));
        StartCoroutine(MoveParticles(particleSystem2, target1));

    }

    private void SwitchHealth()
    {
        float player1Health = knightAttackerController1.healthPercantage;
        knightAttackerController1.healthPercantage = knightAttackerController2.healthPercantage;
        knightAttackerController2.healthPercantage = player1Health;
    }

    private IEnumerator MoveParticles(Transform source, Vector3 target)
    {
        Vector3 initialPosition = source.position;
        Vector3 targetPosition = new Vector3(source.position.x, source.position.y+yOffset, source.position.z);
       
        Vector3 speed = Vector3.zero;
        while (!(Mathf.Abs(source.position.y - targetPosition.y) < 0.1))
        {
            source.position = Vector3.SmoothDamp(source.position, targetPosition, ref speed, initSmoothTime);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(upDuration);
        
        while (!(Vector3.Distance(source.position, target) < 0.1))
        {
            source.position = Vector3.SmoothDamp(source.position, target, ref speed, finalSmoothTime);
            yield return new WaitForEndOfFrame();
        }

        if (shouldExecute)
        {
            var t = target1;
            target1 = target2;
            target2 = t;
            EnableMovement();
            SwitchHealth();
            resetTimerEvent.Invoke();
        }
        else
        {
            shouldExecute = true;
        }
        
    }

    public void DisableMovement()
    {
        knightPlayerController1.IsChangingColor = true;
        knightPlayerController2.IsChangingColor = true;
    }

    public void EnableMovement()
    {
        knightPlayerController1.IsChangingColor = false;
        knightPlayerController2.IsChangingColor = false;
    }

}
