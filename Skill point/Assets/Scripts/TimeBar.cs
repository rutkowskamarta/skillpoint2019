﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeBar : MonoBehaviour
{
    [SerializeField] private Transform foreground;
    [SerializeField] private float timeSpeed;
    [SerializeField] private float resetTimeSpeed;
    [SerializeField] private UnityEvent timerEvent;

    private float initialxscale;

    private void Start()
    {
        initialxscale = foreground.localScale.x;
        StartTimer();
    }

    private IEnumerator ResetTimeBarCoroutine(float speed, Action action)
    {
        
        while (foreground.localScale.x < initialxscale)
        {
            float desiredXLocalScale = foreground.localScale.x + speed;
            
            
            if (desiredXLocalScale > initialxscale)
            {
                desiredXLocalScale = initialxscale;
            }
            var newScale = new Vector3(desiredXLocalScale, foreground.localScale.y, foreground.localScale.z);
            foreground.localScale = Vector3.Lerp(newScale, foreground.localScale, Time.deltaTime) ;
            yield return new WaitForEndOfFrame();
        }
        action.Invoke();
    }

    private IEnumerator TimeBarCoroutine(float speed, Action action)
    {
        while (foreground.localScale.x > 0)
        {
            float desiredXLocalScale = foreground.localScale.x - speed;

            if (desiredXLocalScale < 0)
            {
                desiredXLocalScale = 0;
            }
           
            var newScale = new Vector3(desiredXLocalScale, foreground.localScale.y, foreground.localScale.z);
            foreground.localScale = Vector3.Lerp(newScale, foreground.localScale, Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        action.Invoke();
    }

    private void StartTimer()
    {
        StartCoroutine(TimeBarCoroutine(timeSpeed, () =>
        {
            timerEvent.Invoke();
        }));
    }

    public void ResetTime()
    {
        StartCoroutine(ResetTimeBarCoroutine(resetTimeSpeed, () => StartTimer()));
    }
   
}
